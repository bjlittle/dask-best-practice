#!/usr/bin/bash

# conda execute
# env:
#  - pip
#  - pillow

pip install sphinx
pip install sphinx_rtd_theme
pip install sphinxmark

# Clear the build output from the readthedocs make
make clean

# create the docs
make html
