#!/usr/bin/env bash


# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd)

cd ${SCRIPT_DIR}

# Dump location for built best practices guide.
DOCS_DIR=build
[ -d ${DOCS_DIR} ] && rm -rf ${DOCS_DIR}
mkdir -p ${DOCS_DIR}

# Documentation deployment parent directory.
REMOTE_DIR=~graphics/public_html/sci/dask_best_practice

mkdir -p ${REMOTE_DIR}

# Check the availablity of SCRATCH.
PROBE=$([ -d ${SCRATCH} ] && echo "true" || echo "false")
[ "${PROBE}" == "false" ] && export SCRATCH=${LOCALTEMP}

# Create log file.
LOG_DIR=${SCRATCH}/logs
mkdir -p ${LOG_DIR}
LOG_FILE=${LOG_DIR}/dask_best_practice_build.$(date +%Y-%m-%d-%H%M%S).log
cat <<EOF >${LOG_FILE}
Building Dask Best Practices
DATE:  $(date)
OUTPUT ...
EOF

# The Python script to execute.
BUILD_SCRIPT=build.sh

# The entry-point to conda-execute.
EXECUTE=~avd/live/conda-execute/bin/conda-execute

# Execute the script within a conda-execute controlled environment.
export CONDA_ENVS_PATH=${LOCALTEMP}/conda/publish_dask_best_practice/envs

# Recreate directory tree rooted at CONTENT_DIR at DOCS_DIR with html files built
${EXECUTE} -vf ${BUILD_SCRIPT} >>${LOG_FILE} 2>&1

RETURN_CODE=${?}

# Check exit status of build script.
if [ ${RETURN_CODE} -ne 0 ]
then
    cat ${LOG_FILE}
    echo "Aborted!"
    exit ${RETURN_CODE}
fi

# Copy files to REMOTE_DIR, only updating new and modified files and
# directories
echo "Deploying built pages to ${REMOTE_DIR}" >>${LOG_FILE}
CHANGES=$(rsync -irlpgcoD ${DOCS_DIR}/html/ ${REMOTE_DIR} 2>>${LOG_FILE})
RETURN_CODE=${?}

if [ ${RETURN_CODE} -ne 0 ]
then
    echo ${CHANGES} >>${LOG_FILE}
    cat ${LOG_FILE}
    echo "Aborted!"
    exit ${RETURN_CODE}
fi

# Check for and record any changes.
if [ -n "${CHANGES}" ]
then
    echo ${CHANGES} >>${LOG_FILE}
    echo "Updated" >>${LOG_FILE}
    cat ${LOG_FILE}
else
    echo "Unchanged" >>${LOG_FILE}
fi
