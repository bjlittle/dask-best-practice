.. _chunking:

Chunking
========

Dask breaks down large data arrays into chunks, allowing efficient
parallelisation by processing several smaller chunks simultaneously. For more
information, see the documentation on 
`Dask Array <https://docs.dask.org/en/latest/array.html>`_ 

Iris provides a basic chunking shape to Dask, attempting to set the shape for
best performance. The chunking that is used can depend on the file format that
is being loaded. See below for how chunking is performed for:

  * :ref:`chunking_netcdf` 
  * :ref:`chunking_pp_ff`

It can in some cases be beneficial to re-chunk the arrays in Iris cubes.
For information on how to do this, see `Rechunking
<pp-to-netcdf.html#rechunking>`_.

For an exploration of how Iris works with chunking, see the `Controlling
Iris Data Chunking <iris-data-chunking.html>`_ example


.. _chunking_netcdf:

NetCDF Files
------------

NetCDF files can include their own chunking specification. This is either
specified when creating the file, or is automatically assigned if one or
more of the dimensions is `unlimited <https://www.unidata.ucar
.edu/software/netcdf/docs/unlimited_dims.html>`_.  
Importantly, netCDF chunk shapes are **not optimised for Dask
performance**.

Chunking can be set independently for any variable in a netcdf file.
When a netcdf variable uses an unlimited dimension, it is automatically
chunked: the chunking is the shape of the whole variable, but with '1' instead
of the length in any unlimited dimensions.

When chunking is specified for netcdf data, Iris will set the dask chunking
to an integer multiple or fraction of that shape, such that the data size is
near to but not exceeding the dask array chunk size.

.. Note::
    Prior to Iris 2.3, Iris would not multiply up a chunksize from a netcdf
    variable, which could therefore be quite inefficient.
    Iris 2.2. was thus equally inefficient for some files with unlimited
    dimensions.  Similarly, **Iris 2.2 can often perform badly with StaGE
    data**, as this typically has quite small chunks set in the file.
    Upgrading to Iris 2.3+ can often solve these problems.


.. _chunking_pp_ff:

PP and Fieldsfiles
------------------
PP and Fieldsfiles contain multiple 2D fields of data. When loading PP or 
Fieldsfiles into Iris cubes, the chunking will automatically be set to a chunk
per field. 

For example, if a PP file contains 2D lat-lon fields for each of the
85 model level numbers, it will load in a cube that looks as follows::

    (model_level_number: 85; latitude: 144; longitude: 192)

The data in this cube will be partitioned with chunks of shape 
:code:`(1, 144, 192)`.

If the file(s) being loaded contain multiple fields, this can lead to an 
excessive amount of chunks which will result in poor performance. 

When the default chunking is not appropriate, it is possible to rechunk. 
:ref:`pp-to-netcdf` provides a detailed demonstration of how Dask can optimise
that process.

