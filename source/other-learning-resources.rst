.. _other-learning-resources:

Other Learning Resources
------------------------
To learn more about the topics covered in these Dask Best Practices pages, see 
the below links

* `Iris Documentation <https://scitools.org.uk/iris/docs/latest/>`_
* `Dask Documentation <https://dask.org/>`_
* `Distributed Documentation <https://distributed.dask.org/en/latest/>`_ 
*  `Distributed Python Processing on SPICE <https://metnet2.metoffice.gov.uk/content/distributed-python-processing-spice>`_
* `SPICE user group <https://metnet2.metoffice.gov.uk/team/spice-user-group>`_

For any help with using Dask, please contact AVD via

* AVD Support Mailbox: ml-avd-support@metoffice.gov.uk
* `AVD General Yammer Group <https://www.yammer.com/metoffice.gov.uk/#/threads/inGroup?type=in_group&feedId=10592520>`_
