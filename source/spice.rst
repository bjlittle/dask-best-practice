.. _spice:

Dask on SPICE
=============

SPICE is provided for moderately large tasks, with limited multi-processing.
Thus it is natural to use Dask on SPICE, but here are some important factors
you must be aware of.
In particular, you will *always* need to  explicitly control parallel
operation, both in Dask and likewise in numpy : see sections below.

See the `SPICE User Guide <http://fcm1/projects/metomi/wiki/SPICE/UserGuide>`_
for a full list of SLURM commands


.. _spice-slurm:

SLURM CPU Allocation
--------------------
When running on SPICE, unless configured otherwise, Dask will attempt to create
one parallel 'worker' task for each CPU visible on the node (48).
However, within a Slurm allocation, only *some* of these CPUs are actually
accessible -- often, and by default, only one.
This leads to a serious over-commitment unless it is controlled.

So, **whenever Iris is used on SPICE, you must always control the number
of dask workers to a sensible value**, matching the slurm allocation.  You do
this with ::

    dask.config.set(num_workers=N)

For an example, see :ref:`greedy-parallelism`.

Alternatively, when there is only one CPU allocated, it may actually be more
efficient to use a "synchronous" scheduler instead, with ::

    dask.config.set(scheduler='synchronous')

See `Single Thread 
<https://docs.dask.org/en/latest/scheduling.html?highlight=single-threaded#single-thread>`_ .


.. _spice-numpy:

Numpy Threading
---------------
Numpy also interrogates the visible number of CPUs to multi-thread its
operations.
The large number of CPU's available in SPICE will thus cause confusion if Numpy
attempts its own parallelisation, so this must be prevented. See
:ref:`numpy-threads` for detail



Distributed
-----------
Even though SPICE allocations are generally restricted to a single node, there
are still good reasons for using 'dask.distributed' in many cases.
See `Single Machine: dask.distributed 
<https://docs.dask.org/en/latest/setup/single-distributed.html>`_ in the
Dask documentation. For a detailed guide to using ``dask.distributed`` on
SPICE see `Distributed Python processing on SPICE
<https://metnet2.metoffice.gov.uk/content/distributed-python-processing-spice>`_
on MetNet.


