.. _numpy-threads:

Numpy Threads
=============

In certain scenarios numpy will attempt to perform threading using an
external library - typically OMP, MKL or openBLAS - making use of **every**
CPU available. This interacts badly with Dask:

* Dask may create multiple instances of numpy, each generating enough
  threads to use **all** the available CPU's. The resulting sharing of CPU's
  between threads greatly reduces performance. The more cores there are, the
  more pronounced this problem is.
* Numpy will generate enough threads to use all available CPU's even
  if Dask is deliberately configured to only use a subset of CPU's. The
  resulting sharing of CPU's between threads greatly reduces performance.
* `Dask is already designed to parallelise with numpy arrays <https://docs
  .dask.org/en/latest/array.html>`_, so adding numpy's 'competing' layer of
  parallelisation could cause unpredictable performance.

Therefore it is best to prevent numpy performing its own parallelisation, `a
suggestion made in Dask's own documentation <https://docs.dask
.org/en/stable/array-best-practices.html#avoid-oversubscribing-threads>`_.
The following commands will ensure this in all scenarios:

in Python...

::

    # Must be run before importing numpy.
    import os
    os.environ["OMP_NUM_THREADS"] = "1"
    os.environ["OPENBLAS_NUM_THREADS"] = "1"
    os.environ["MKL_NUM_THREADS"] = "1"
    os.environ["VECLIB_MAXIMUM_THREADS"] = "1"
    os.environ["NUMEXPR_NUM_THREADS"] = "1"

or in Linux command line...

::

    export OMP_NUM_THREADS=1
    export OPENBLAS_NUM_THREADS=1
    export MKL_NUM_THREADS=1
    export VECLIB_MAXIMUM_THREADS=1
    export NUMEXPR_NUM_THREADS=1
