Dask Best Practices
===================

.. toctree::
    :maxdepth: 2
    :hidden:
    :caption: Guidance

    numpy-threads
    spice
    chunking

.. toctree::
    :maxdepth: 2
    :hidden:
    :numbered:
    :caption: Examples

    pp-to-netcdf
    loop-third-party
    greedy-parallelism

.. toctree::
    :maxdepth: 2
    :hidden:
    :caption: Further Reading

    other-learning-resources
 
.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Related Documentation

   Scientific Software Stack <http://www-avd/sci/software_stack/>



`Dask <https://dask.org/>`_ is a powerful tool for speeding up data handling
via lazy loading and parallel processing. To get the full benefit of using
Dask, it is important to configure it correctly and supply it with
appropriately structured data. For example, we may need to "chunk" data arrays
into smaller pieces to process, read and write it; getting the "chunking" right
can make a significant different to performance!

To make sure you get the most out of Dask, check the list of pages on the left 
for those that seem relevant to you.

In particular, you may find the following useful

  * `Numpy Threads <numpy-threads.html>`_ for preventing numpy gobbling up your
    compute resources
  * `NetCDF Files <chunking.html#netcdf-files>`_ or 
    `PP and Fieldsfiles <chunking.html#pp-and-fieldsfiles>`_  post for more on the 
    importance of setting appropriate chunking.

.. warning::
    This document is still being developed!  
    
    Here, we have collated a handful of examples that we hope will assist 
    users to make the best start when learning to use Dask.  It is *not* a 
    fully comprehensive guide encompassing all best practices.  Our intention
    *is* to further expand and develop the content, but we hope that by 
    sharing the real world examples we currently have may help others now.

    Please provide your feedback by emailing 
    ml-avd-support@metoffice.gov.uk




