# Dask Best Practices Documentation

Guidance and examples of best practices for use of Dask, written in 
[reStructuredText](https://en.wikipedia.org/wiki/ReStructuredText) (`.rst` 
files), built using [Sphinx](http://www.sphinx-doc.org/en/master/)

## Folder Structure

The individual pages (`.rst` files) are in the `/source` folder. Any images 
attached to these pages are in `/source/images`

Scripts for environment creation and file building are in the top directory

When Sphinx builds from the `/source` files, these are stored in the `/build` 
folder. The `.gitignore` file in the top directory is set to ignore the 
`/build` folder and its contents

## Building

To build, you will need an environment containing Sphinx. This can be done 
using conda-execute to create a temporary environment and run the build.

First, add a location that the temporary_environment will live:

    export CONDA_ENVS_PATH=${LOCALTEMP}/conda/publish_dask_best_practice/envs

Then run the `build.sh` script using the centrally installed conda-execute. 

    ~avd/live/conda-execute/bin/conda-execute -v build.sh

Note, you can use the `-f` flag to force conda-execute to re-create the 
environment even if it exists.

Specific instructions for how Sphinx should behave are in the `Makefile` in 
the top directory

## Viewing Built Site

You can view the contents of `/build` in website form by visiting the 
`/build/html/index.html` in a web browser. E.g.

    firefox build/html/index.html

If using a scientific desktop setup, you can create a share-able website 
using a symbolic link to your personal web-space:

    ln -s build/html/ ~/public_html/dask-best-practices

(your `~public_html/` corresponds to an equivalent of 
`http://www-<team-code>/~<linux-user-name>/`, e.g. `http://www-avd/~myeo/`)
